       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MYGRADE.
       AUTHOR. HOLMES.
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FLIE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVERAGE-FLIE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FLIE.
       01  GRADE-DETAIL.
           88 END-OF-GRADE-FILE   VALUE HIGH-VALUE.
           05 COURSE-CODE PIC   X(6).
           05 COURSE-NAME PIC   X(50).
           05 CREDIT      PIC   9.
           05 GRADE       PIC   XX.

       FD  AVERAGE-FLIE.
       01  AVERAGE-DETAIL.
           05 AVG-GRADE.      
              10 AVG-GRADE-NAME       PIC X(18).
              10 ALL-GRADE.            
                 15 ALL-GRADE-FRONT   PIC 9.
                 15 ALL-GRADE-DOT     PIC X.
                 15 ALL-GRADE-BACK    PIC V999.
              10 ALL-COMMA            PIC X.
           05 AVG-SCI-GRADE.
              10 AVG-SCI-GRADE-NAME   PIC X(18).
              10 SCI-GRADE.     
                 15 SCI-GRADE-FRONT   PIC 9.
                 15 SCI-GRADE-DOT     PIC X.
                 15 SCI-GRADE-BACK    PIC V999.
               10 SCI-COMMA           PIC X.
           05 AVG-CS-GRADE.
              10 AVG-CS-GRADE-NAME    PIC X(18).
              10 CS-GRADE.
                 15 CS-GRADE-FRONT    PIC 9.
                 15 CS-GRADE-DOT      PIC X.
                 15 CS-GRADE-BACK     PIC V999.
              
       WORKING-STORAGE SECTION. 
       01  CREDIT-ALL-GRADE   PIC 999.
       01  CREDIT-SCI-GRADE   PIC 999.
       01  CREDIT-CS-GRADE    PIC 999.
       01  SCORE              PIC 99V9.
       01  SUM-SCORE-ALL      PIC 999v9.
       01  SUM-SCORE-SCI      PIC 999v9.
       01  SUM-SCORE-CS       PIC 999v9.
       01  AVG                PIC 9v999.
       01  COURSE             PIC XX.
           88 SCIENCE         VALUE "30" THRU "31".
           88 CS              VALUE "31".
                    

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT GRADE-FLIE
           OPEN OUTPUT  AVERAGE-FLIE
           
           PERFORM  UNTIL END-OF-GRADE-FILE 
              READ GRADE-FLIE
                 AT END SET END-OF-GRADE-FILE TO TRUE
              END-READ 

              IF NOT END-OF-GRADE-FILE THEN
                 MOVE COURSE-CODE TO COURSE
                 PERFORM 002-SELECT-ALL THRU 002-SELECT-CS 
              END-IF  

           END-PERFORM

           PERFORM 003-CALCULATE-ALL THRU 003-CALCULATE-CS
          
           DISPLAY AVERAGE-DETAIL.
           WRITE AVERAGE-DETAIL.
           
           CLOSE GRADE-FLIE 
           CLOSE AVERAGE-FLIE
           GOBACK .

       001-PR0CESS.
           EVALUATE TRUE
             WHEN GRADE = "A" MOVE 4 TO SCORE
             WHEN GRADE = "B+" MOVE 3.5 TO SCORE
             WHEN GRADE = "B" MOVE 3 TO SCORE
             WHEN GRADE = "C+" MOVE 2.5 TO SCORE
             WHEN GRADE = "C" MOVE 2 TO SCORE
             WHEN GRADE = "D+" MOVE 1.5 TO SCORE
             WHEN GRADE = "D" MOVE 1 TO SCORE
             WHEN OTHER MOVE 0 TO SCORE
            END-EVALUATE.
       
       002-SELECT-ALL.
           PERFORM 001-PR0CESS
                 COMPUTE CREDIT-ALL-GRADE = CREDIT-ALL-GRADE+ CREDIT
                 COMPUTE SCORE = SCORE * CREDIT 
                 COMPUTE SUM-SCORE-ALL  = SUM-SCORE-ALL  +SCORE.
       
       002-SELECT-SCI.
           IF SCIENCE  
                 PERFORM 001-PR0CESS
                 COMPUTE CREDIT-SCI-GRADE = CREDIT-SCI-GRADE+ CREDIT
                 COMPUTE SCORE = SCORE * CREDIT
                 COMPUTE SUM-SCORE-SCI   = SUM-SCORE-SCI   +SCORE
                 END-IF.

       002-SELECT-CS.
           IF CS  
                 PERFORM 001-PR0CESS
                 COMPUTE CREDIT-CS-GRADE = CREDIT-CS-GRADE+ CREDIT
                 COMPUTE SCORE = SCORE * CREDIT
                 COMPUTE SUM-SCORE-CS   = SUM-SCORE-CS   +SCORE
                 END-IF.

       003-CALCULATE-ALL.
           MOVE "AVG-GRADE : " TO AVG-GRADE-NAME
           COMPUTE AVG   = SUM-SCORE-ALL/CREDIT-ALL-GRADE
           MOVE AVG TO ALL-GRADE-FRONT
           MOVE "." TO ALL-GRADE-DOT 
           MOVE AVG TO ALL-GRADE-BACK
           MOVE "," TO ALL-COMMA.
         
       003-CALCULATE-SCI.
           MOVE " AVG-SCI-GRADE : " TO AVG-SCI-GRADE-NAME
           COMPUTE AVG   = SUM-SCORE-SCI /CREDIT-SCI-GRADE
           MOVE AVG TO SCI-GRADE-FRONT
           MOVE "." TO SCI-GRADE-DOT 
           MOVE AVG TO SCI-GRADE-BACK
           MOVE "," TO SCI-COMMA.
       
       003-CALCULATE-CS.
           MOVE " AVG-CS-GRADE : " TO AVG-CS-GRADE-NAME
           COMPUTE AVG   = SUM-SCORE-CS /CREDIT-CS-GRADE
           MOVE AVG TO CS-GRADE-FRONT
           MOVE "." TO CS-GRADE-DOT 
           MOVE AVG TO CS-GRADE-BACK.
